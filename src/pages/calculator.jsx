import React, { useState } from 'react';
import Display from '../components/display';
import Keyboard from '../components/keyboard';
import '../styles/calculator.css';

const Calculator = () => {
    const [firstNumber, setFirstNumber] = useState(null);
    const [secondNumber, setSecondNumber] = useState(null);
    const [operator, setOperator] = useState(null);
    const [result, setResult] = useState(null);

    const resetCalc = () => {
        setFirstNumber(null);
        setSecondNumber(null);
        setOperator(null);
        setResult(null);
    };

    const calcResult = () => {
        if (!firstNumber || !secondNumber || !operator) {
            return;
        }
        if(operator === '/'){
            if(secondNumber==0){
                setResult('Err');
                setTimeout(()=>{
                    resetCalc()
                },1000)
                return
            }
            if(firstNumber === 0){
                setResult(0);
                return;
            }
        }
        setResult(eval(`${firstNumber}${operator}${secondNumber}`));
    };

    const invertNumber = () => {
        
        if (firstNumber && secondNumber && result) {
            setFirstNumber(result * (-1));
            setSecondNumber(null);
            setResult(null);
            setOperator(null);
            return;
        }
        if (!operator && firstNumber) {
            setFirstNumber(firstNumber * (-1));
            return;
        }
        if (secondNumber) {
            setSecondNumber(secondNumber * (-1));
            return;
        }
    };

    const addNumber = (num) => {
        if (!operator || (firstNumber && secondNumber && result)) {
            resetCalc();
            setFirstNumber(`${num}`);
            return;
        }
        if (!operator) {
            setFirstNumber(`${(!firstNumber) ? '' : firstNumber}${num}`);
            return
        }
        setSecondNumber(`${(!secondNumber) ? '' : secondNumber}${num}`);
    };

    const setOperation = (operator) => {
        if (firstNumber && secondNumber) {
            setFirstNumber(result);
            setSecondNumber(null);
            setResult(null);
        }
        setOperator(operator);
    };

    return (
        <div className="container">
            <Display
                firstNumber={firstNumber}
                operator={operator}
                secondNumber={secondNumber}
                result={result}
            />
            <Keyboard
                resetCalc={resetCalc}
                calcResult={calcResult}
                invertNumber={invertNumber}
                addNumber={addNumber}
                setOperation={setOperation}
            />
        </div>
    )
}

export default Calculator