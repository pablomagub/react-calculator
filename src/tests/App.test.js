import { render, screen } from "@testing-library/react"
import userEvent from "@testing-library/user-event"
import App from '../App'

it('should see the calculator', () => {
    render(<App/>)

    const accumulator = screen.getByText('AC')

    expect(accumulator).toBeInTheDocument()
})

it('should see the presed number', () => {
    render(<App/>)

    const number = screen.getByText('9')
    userEvent.click(number)
    const display = screen.getByLabelText('display number')

    expect(display).toHaveTextContent('9')
})

it.only('should num tow numbers', () => {
    render(<App/>)

    const firstNumber = screen.getByText('9')
    userEvent.click(firstNumber)
    const operator = screen.getByText('+')
    userEvent.click(operator)
    const secondNumber = screen.getByText('2')
    userEvent.click(secondNumber)
    const result = screen.getByText('=')
    userEvent.click(result)
    const display = screen.getByLabelText('display number')

    expect(display).toHaveTextContent('18')
})
